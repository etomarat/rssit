# coding: utf-8

import os.path


# init

SITE_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = lambda *args: os.path.join(SITE_ROOT, *args)


DEBUG = True
TEMPLATE_DEBUG = DEBUG


ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    )

MANAGERS = ADMINS


ALLOWED_HOSTS = []

SITE_ID = 1

SECRET_KEY = 'z$4x4$zc2$d_fs55y0wc%c=7&-szgr(63a%69s2xx0e!5yg4rq'

ROOT_URLCONF = 'rssit.urls'

WSGI_APPLICATION = 'rssit.wsgi.application'

# /init

# databases

DB_BLANK = {
    'ENGINE': '',
    'NAME': '',
    'USER': '',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '',
    }

DB_SQLITE = {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': os.path.join(SITE_ROOT, 'data.db').replace('\\', '/')
    }

DATABASES = {
    'default': DB_SQLITE,
    }

# /databases




# locales

TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru'

USE_I18N = False

USE_L10N = True

USE_TZ = True

# /locales



# media

MEDIA_ROOT = path('media/')

MEDIA_URL = '/media/'

# /media
# static

STATIC_ROOT = path('www/static')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    path('static/'),
    )

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
    )

# /static

# templates

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
    )

TEMPLATE_DIRS = (
    path('templates/'),
    )

# /templates

# apps

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # 'django.contrib.admindocs',
    
    'south',
    
    'rssit.core',
)

# /apps

# socia auth

#LOGIN_URL = '/'
#LOGIN_REDIRECT_URL = '/friend_list/'
# LOGIN_ERROR_URL 


AUTHENTICATION_BACKENDS = (
    # 'social_auth.backends.facebook.FacebookBackend',
    #'social_auth.backends.contrib.vk.VKOAuth2Backend',
    # 'social_auth.backends.contrib.odnoklassniki.OdnoklassnikiBackend',
    'django.contrib.auth.backends.ModelBackend',
    )

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    #'social_auth.context_processors.social_auth_by_name_backends',
    )

"""
SOCIAL_AUTH_PIPELINE = (
    # Получает по backend и uid инстансы social_user и user
    'social_auth.backends.pipeline.social.social_auth_user',
    # Получает по user.email инстанс пользователя и заменяет собой тот, который получили выше.
    # Кстати, email выдает только Facebook и GitHub, а Vkontakte и Twitter не выдают
    # 'social_auth.backends.pipeline.associate.associate_by_email',
    # Пытается собрать правильный username, на основе уже имеющихся данных
    'social_auth.backends.pipeline.user.get_username',
    # Создает нового пользователя, если такого еще нет
    'social_auth.backends.pipeline.user.create_user',
    # '~~~~~~~~~~~~~~~~~~~~~~~~~~.registration.social_api.pipeline.create_user',
    # Пытается связать аккаунты
    'social_auth.backends.pipeline.social.associate_user',
    # Получает и обновляет social_user.extra_data
    'social_auth.backends.pipeline.social.load_extra_data',
    # Обновляет инстанс user дополнительными данными с бекенда
    'social_auth.backends.pipeline.user.update_user_details'
    # '~~~~~~~~~~~~~~~~~~~~~~~~~~.registration.social_api.pipeline.update_user_details',
    )


VK_APP_ID = '3928845'
VK_API_SECRET = '2T3RqoMohhbE8XljJBcN'
VKONTAKTE_APP_ID = VK_APP_ID
VKONTAKTE_APP_SECRET = VK_API_SECRET
VK_EXTRA_SCOPE = ['wall', 'friends']

# /socia auth"""

# other

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# EOF
