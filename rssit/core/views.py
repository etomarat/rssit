# Create your views here.
import os

from django.template.context import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response

def www_return (request, filename):
    filename = filename.encode('utf')
    f = open('www_response/'+filename, 'rb')
    return HttpResponse(f)

def wget (request):
    if request.POST.get('q'):
        q = request.POST.get('q')
        url = q.split('//')[1]
        print url
        if url[-1] == '/':
            url = url+'index'
    cmd = 'wget -r -k -l 1 -p -E -nc -P ./www_response/ ' + q
    #cmd = 'ls'
    os.system(cmd)
    return render_to_response('selector.html', {'url': url}, RequestContext(request))